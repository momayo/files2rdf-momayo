<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    exclude-result-prefixes="xs"
    version="2.0">
    <!-- Small stylesheet that takes a digital_resource full ontology (saved as rdf/xml) and delivers out a lighter version where you just get thumbnail triples to see in protege-->
    <!--@param ignored_resourcenames is classes to ignore-->
    <xsl:param name="ignored_resourcenames" select="('http://www.openarchives.org/ore/terms/Aggregation','http://purl.org/momayo/mmdr/DigitalResource','http://purl.org/momayo/mmdr/Canvas')"/>
    <!-- @param ontology_name full ontology name_uri ex: http://data.ub.uib.no/dataset/dr_min_bs.owl-->
    <xsl:param name="ontology_name"/>
    <xsl:strip-space elements="*"/>
    <xsl:output indent="yes"/>
    
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="rdf:RDF">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <!-- copies stylesheet and adds ontology from $ontology_name-->
            <xsl:attribute name="xml:base" select="$ontology_name"/>
            <rdf:Description rdf:about="">  
                <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#Ontology"/>
            </rdf:Description>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*" priority="2.0">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>   
    
    <!-- ignores classes where $ignored resourcename is rdf:type-->    
    <!-- ignored specific from dr_ontology connected to item (rdf:type exists, and hasRepresenation would lead to external resources-->   
    <xsl:template match="rdf:Description[rdf:type/@rdf:resource=$ignored_resourcenames]|*:hasRepresentation | rdf:type" priority="3.0"/>
</xsl:stylesheet>
