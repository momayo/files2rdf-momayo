# Files2Rdf
Mapping fra filsystem til ubbont rdf (digital representation). Kobler på og skriver
rdf for filer som har samme identifier på filer som i Marcus. Genererer fortsatt rdf for aggregation, page og Digital Resource for objekter som enda ikke finnes i Marcus. 
[Lenke til createfilelist]

`@input file` resultatfil av `createfilelist.xsl`. 
`@params`

- `lookup` path til sparql resultat xml fil, med identifiers og `rdf:about`
- `debug` boolean for messages for debugging. 
 
 
## Beskrivelse av mapping

Namespace for digitaleressurser properties og klasses er
`xmlns:mmdr="http://purl.org/momayo/mmdr/"`


```
<ex:Document> 
<bibo:pages> N ^xs:integer;
<mmdr:hasRepresentation> <Aggregation>;
<mmdr:hasThumbnail> "thumbnail" xs:anyURI.

#Legg inn katalogisert? harDZI? i datasettet for å forenkle indekserings-skript

<ore:Aggregation>
<mmdr:hasThumbnail> "thumbnail" xs:anyURI;
<mmdr:isRepresentationOf> <ex:Document>; 
<mmdr:hasItem> <mmdr:Canvas> .

<mmdr:Canvas>
<mmdr:isItemOf> <mmdr:Aggregation>;
<mmdr:hasThumbnail> "thumbnail" xs:anyURI;
<mmdr:previousPage> <prevPage>;
<mmdr:nextPage> <nextPage>;
<mmdr:sequenceNr> N in sequence xs:integer ;
<rdfs:label> "sidenavn";
<mmdr:hasResource> <mmdr:DigitalResource>. 

<mmdr:DigitalResource>
<mmdr:hasDZIView> "DziView";
<mmdr:HasTHView> "View";
<mmdr:HasSMView> "View";
<mmdr:HasMDView> "View";
<mmdr:HasLGView> "View";
<mmdr:HasXLView> "View";
<mmdr:HasXSView> "View";
<mmdr:hasView "View" #foreløbig brukt for PDF
<mmdr:isResourceOf> <mmdr:Canvas>, <ore:Aggregation>;
#<ubbont:quality> ikke i bruk men mappet;
<rdfs:label> "Label" .
```
## rdfFile2fileOntology.xsl
Tar en resultatfil fra files2rdf.xsl og klargjør resultatfil for Protege. Brukt for å få inn thumbnails av bilder på enkeltdokumenter i Protege.
Fil må navngis for import i hovedprosjektsfil i.e:  `marcus.rdf-xml.owl.`
